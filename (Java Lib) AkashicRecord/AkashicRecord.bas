﻿Type=Class
Version=4.7
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Class_Globals
	Private SQL1 As SQL
	Private Ser As B4XSerializator
	#If B4i
	Private C As Cipher
	#Else
	Private C As B4XCipher
	#End If
	#If B4J
	Private fx As JFX
	#End If
End Sub

'Initializes the Universe and sets the Universe file.
Public Sub Initialize (Dir As String, FileName As String)
	If FileName.Contains(".akashic") = False Then
		FileName = FileName & ".akashic"
	End If
	If SQL1.IsInitialized Then SQL1.Close
	#If B4J
	SQL1.InitializeSQLite(Dir, FileName, True)
	#Else
	SQL1.Initialize(Dir, FileName, True)
	#End If
	CreateAkashicRecord
End Sub

#Region "Record Actions"
Private Sub CreateAkashicRecord
	SQL1.ExecNonQuery("CREATE TABLE IF NOT EXISTS Main (Key TEXT PRIMARY KEY, Value NONE)")
	SQL1.ExecNonQuery("CREATE TABLE IF NOT EXISTS Akashic_Settings (Setting TEXT PRIMARY KEY, Value NONE)")
End Sub

'Clears the AkashicRecord.
Public Sub Clear
	SQL1.ExecNonQuery("DROP TABLE Main")
	CreateAkashicRecord
End Sub

'Closes the AkashicRecord.
Public Sub Close
	SQL1.Close
End Sub
#End Region


#Region "Main Actions"
Public Sub Put (Key As String, Value As Object)
	SQL1.ExecNonQuery2("REPLACE INTO Main VALUES(?, ?)", Array As Object(Key, Ser.ConvertObjectToBytes(Value)))
End Sub

Public Sub Get (Key As String) As Object
	Dim TheResult As Object = Null
	Dim RS As ResultSet = SQL1.ExecQuery2("SELECT Value FROM Main WHERE Key = ?", Array As String(Key))
	Do While RS.NextRow
		TheResult = RS.GetBlob2(0)
	Loop
	Return Ser.ConvertBytesToObject(TheResult)
End Sub

'Alters a Key.
Public Sub AlterKey (OldKey As String, NewKey As String)
	SQL1.ExecNonQuery2("UPDATE Main SET Key = ? WHERE Key = ?", Array As String(NewKey, OldKey))
End Sub

'Alters  an already existing value.
Public Sub AlterValue (Key As String, NewValue As Object)
	DeleteKey(Key)
	Put(Key, NewValue)
End Sub

Public Sub AlterValueAndEncrypt (Key As String, NewValue As Object, Password As String)
	DeleteKey(Key)
	PutEncrypted(Key, NewValue, Password)
End Sub

'Deletes the provided Key and Value pair.
'WARNING: This is not reversible! I am not responsible for any data loss that results from the usage of this method!
Public Sub DeleteKey (Key As String)
	SQL1.ExecNonQuery2("DELETE FROM Main WHERE Key = ?", Array As String(Key))
End Sub

'Returns a list of Keys
Public Sub ListKeys As List
	Dim TheResult As List
	TheResult.Initialize
	Dim RS As ResultSet = SQL1.ExecQuery("SELECT Key FROM Main")
	Do While RS.NextRow
		TheResult.Add(RS.GetString2(0))
	Loop
	Return TheResult
End Sub
#End Region

#Region "Sub-Actions"
Public Sub PutEncrypted (Key As String, Value As Object, Password As String)
	Put(Key, C.Encrypt(Ser.ConvertObjectToBytes(Value), Password))
End Sub

#If B4J
Public Sub PutImage (Key As String, Value As Image)
	Dim Output As OutputStream
	Output.InitializeToBytesArray(0)
	Value.WriteToStream(Output)
	Put(Key, Output.ToBytesArray)
	Output.Close
End Sub

Public Sub PutImageEncrypted (Key As String, Value As Image, Password As String)
	Dim Output As OutputStream
	Output.InitializeToBytesArray(0)
	Value.WriteToStream(Output)
	PutEncrypted(Key, Output.ToBytesArray, Password)
	Output.Close
End Sub
#Else
Public Sub PutBitmap (Key As String, Value As Bitmap)
	Dim Output As OutputStream
	Output.InitializeToBytesArray(0)
	Value.WriteToStream(Output, 0, "PNG")
	Put(Key, Output.ToBytesArray)
	Output.Close
End Sub

Public Sub PutBitmapEncrypted (Key As String, Value As Bitmap, Password As String)
	Dim Output As OutputStream
	Output.InitializeToBytesArray(0)
	Value.WriteToStream(Output, 0, "PNG")
	PutEncrypted(Key, Output.ToBytesArray, Password)
	Output.Close
End Sub
#End If

Public Sub GetEncrypted (Key As String, Password As String) As Object
	Dim B() As Byte = Get(Key)
	If B = Null Then Return Null
	Return Ser.ConvertBytesToObject(C.Decrypt(B, Password))
End Sub

#If B4J
Public Sub GetImage (Key As String) As Image
	Dim B() As Byte = Get(Key)
	If B = Null Then Return Null
	Dim Input As InputStream
	Input.InitializeFromBytesArray(B, 0, B.Length)
	Dim Img As Image
	Img.Initialize2(Input)
	Input.Close
	Return Img	
End Sub

Public Sub GetImageEncrypted (Key As String, Password As String) As Image
	Dim B() As Byte = GetEncrypted(Key, Password)
	If B = Null Then Return Null
	Dim Input As InputStream
	Input.InitializeFromBytesArray(B, 0, B.Length)
	Dim Image As Image
	Image.Initialize2(Input)
	Input.Close
	Return Image	
End Sub
#Else
Public Sub GetBitmap (Key As String) As Bitmap
	Dim B() As Byte = Get(Key)
	If B = Null Then Return Null
	Dim Input As InputStream
	Input.InitializeFromBytesArray(B, 0, B.Length)
	Dim Bmp As Bitmap
	Bmp.Initialize2(Input)
	Input.Close
	Return Bmp	
End Sub

Public Sub GetBitmapEncrypted (Key As String, Password As String) As Bitmap
	Dim B() As Byte = GetEncrypted(Key, Password)
	If B = Null Then Return Null
	Dim Input As InputStream
	Input.InitializeFromBytesArray(B, 0, B.Length)
	Dim Bmp As Bitmap
	Bmp.Initialize2(Input)
	Input.Close
	Return Bmp	
End Sub
#End If
#End Region